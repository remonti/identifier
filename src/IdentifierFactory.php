<?php

declare(strict_types=1);

namespace Demliz\DocumentIdentifier;

use Demliz\DocumentIdentifier\Exception\UnrecognizedIdentifierException;
use Demliz\DocumentIdentifier\Identifier\IdentifierInterface;
use Demliz\DocumentIdentifier\Identifier\NebDocumentIdentifier;
use Demliz\DocumentIdentifier\Identifier\OekDocumentIdentifier;
use Demliz\DocumentIdentifier\Identifier\RslDocumentIdentifier;

/**
 * Класс для автоматического определения типа идентификатора и создания на его основе соответствующего объекта
 *
 * @author Vladimir Chalenko <rezikovka@demliz.com>
 */
final class IdentifierFactory
{
    /**
     * Метод определяет принадлежность идентификатора и возвращает соответствующий этому типу объект
     *
     * @param string $identifier строка, содержащая идентификатор, который нужно обработать
     * @return IdentifierInterface
     * @throws UnrecognizedIdentifierException в случае, если переданный идентификатор не распознан
     */
    public static function create(string $identifier): IdentifierInterface
    {
        $identifierEntity = null;

        if (RslDocumentIdentifier::match($identifier)) {
            $identifierEntity = new RslDocumentIdentifier($identifier);
        } elseif (OekDocumentIdentifier::match($identifier)) {
            $identifierEntity = new OekDocumentIdentifier($identifier);
        } elseif (NebDocumentIdentifier::match($identifier)) {
            $identifierEntity = new NebDocumentIdentifier($identifier);
        }

        if (!$identifierEntity) {
            throw new UnrecognizedIdentifierException('Не удалось распознать идентификатор.');
        }

        return $identifierEntity;
    }
}
