<?php

declare(strict_types=1);

namespace Demliz\DocumentIdentifier\Identifier;

/**
 * Интерфейс идентификатора документа
 *
 * @author Chalenko Vladimir <rezikovka@demliz.com>
 */
interface IdentifierInterface
{
    /**
     * Метод содержит проверку соответствия формата идентификатора требуемому и возвращет флаг успеха
     *
     * @param string $identifier строковое значение идентификатора, которое требуется проверить на соответствие
     * @return bool
     */
    public static function match(string $identifier): bool;

    /**
     * Метод возвращает полный идентификатор документа
     *
     * @return string
     */
    public function getIdentifier(): string;
}
