<?php

declare(strict_types=1);

namespace Demliz\DocumentIdentifier\Identifier;

use Assert\Assertion;

/**
 * Класс, экземпляр которого идентифицирует документ из каталога НЭБ
 *
 * @author Vladimir Chalenko <rezikovka@demliz.com>
 */
final class NebDocumentIdentifier implements IdentifierInterface
{
    /**
     * Регулярное выражение, которому должен соответствовать формат идентификатора
     */
    private const IDENTIFIER_REGEXP = '/^[\w-]+$/';

    /**
     * @var string идентификатор документа из библиотеки НЭБ
     */
    private $identifier;

    /**
     * Конструктор
     *
     * @param string $identifier идентификатор документа, для которого требуется создать экземпляр класса
     */
    public function __construct(string $identifier)
    {
        Assertion::true(self::match($identifier), 'Идентификатор не соответствует требуемому формату.');
        $this->identifier = $identifier;
    }

    /**
     * @inheritdoc
     */
    public static function match(string $identifier): bool
    {
        return (bool) preg_match(self::IDENTIFIER_REGEXP, $identifier);
    }

    /**
     * @inheritdoc
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }
}
