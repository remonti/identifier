<?php

declare(strict_types=1);

namespace Demliz\DocumentIdentifier\Identifier;

/**
 * Интерфейс для классов идентификаторов, значение которых делится на код каталога
 * и непосредственно идентификатор документа
 *
 * @author Vladimir Chalenko <rezikovka@demliz.com>
 */
interface CatalogAwareIdentifierInterface
{
    /**
     * Метод возвращает код каталога
     *
     * @return string
     */
    public function getCatalogCode(): string;

    /**
     * Метод возвращает идентификатор документа
     *
     * @return string
     */
    public function getDocumentId(): string;
}
