<?php

declare(strict_types=1);

namespace Demliz\DocumentIdentifier\Exception;

use RuntimeException;

/**
 * Исключение, которое выбрасывается в случае, если переданный идентификатор не был распознан
 *
 * @author Vladimir Chalenko <rezikovka@demliz.com>
 */
final class UnrecognizedIdentifierException extends RuntimeException
{
}
