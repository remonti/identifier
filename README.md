### Библиотека для работы с идентификаторами документов

##### 1. Установка
Пакет можно установить через Composer:
```
$ composer require demliz/document-identifier
```
##### 2. Использование
Все классы идентификаторов реализованы на основе единого интерфейса 
`Demliz\DocumentIdentifier\Identifier\IdentifierInterface`.  
Существует два пути использования библиотеки:

 1) Прямая инициализация требуемого объекта. Например, у вас есть идентификатор формата `rsl01000000002`. 
 Для этого формата идентификатора существует класс `Demliz\DocumentIdentifier\Identifier\RslDocumentIdentifier`.
 Чтобы инициализировать объект, нужно просто создать экземпляр класса, передав идентификатор в качестве
 аргумента в констуктор:
    ```
    <?php 
    
    use Demliz\DocumentIdentifier\Identifier\RslDocumentIdentifier;
    use Exception;
    
    class SomeClass
    {
        public function someMethod()
        {
            $identifier = 'rsl01000000002';
            if (!RslDocumentIdentifier::match($identifier)) {
                throw new Exception('Некорректный формат идентификатора.');
            }
    
            $identifierEntity = new RslDocumentIdentifier($identifier);
            ...
        }
    }
    ```
 2) Автоопределение типа идентификатора и получение соответствующего объекта с помощью класса 
 `Demliz\DocumentIdentifier\IdentifierFactory`. Пример использования механизма:
    ```
    <?php 
        
    use Demliz\DocumentIdentifier\IdentifierFactory;
    use Demliz\DocumentIdentifier\Exception\UnrecognizedIdentifierException;
    
    class SomeClass
    {
        public function someMethod()
        {
            $identifier = 'rsl01000000002';
            try {
               $identifierEntity = IdentifierFactory::create($identifier);
               ...
            } catch (UnrecognizedIdentifierException $exception) {
               ...
            }
        }
    }
    ```
