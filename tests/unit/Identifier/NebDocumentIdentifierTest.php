<?php

declare(strict_types=1);

namespace Demliz\DocumentIdentifier\Tests\Identifier;

use Assert\InvalidArgumentException;
use Codeception\Test\Unit;
use Demliz\DocumentIdentifier\Identifier\NebDocumentIdentifier;

/**
 * Тест для класса идентификатора НЭБ
 *
 * @author Vladimir Chalenko <rezikovka@demliz.com>
 */
class NebDocumentIdentifierTest extends Unit
{
    /**
     * Проверка получения объекта идентификатора документа НЭБ в случае передачи корректного аргумента в конструктор
     *
     * @dataProvider correctIdentifiersDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\NebDocumentIdentifier::__construct()
     * @param string $nebFullDocumentId полный идентификатор документа в формате НЭБ
     */
    public function testConstruct(string $nebFullDocumentId): void
    {
        $this->assertInstanceOf(
            NebDocumentIdentifier::class,
            new NebDocumentIdentifier($nebFullDocumentId)
        );
    }

    /**
     * Проверка получения объекта идентификатора документа НЭБ в случае передачи некорректного аргумента в конструктор
     *
     * @dataProvider invalidIdentifiersDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\NebDocumentIdentifier::__construct()
     * @param string $nebFullDocumentId некорректный идентификатор документа в формате НЭБ
     */
    public function testConstructWithInvalidParam(string $nebFullDocumentId): void
    {
        $this->expectException(InvalidArgumentException::class);
        new NebDocumentIdentifier($nebFullDocumentId);
    }

    /**
     * Проверка метода валидации идентификатора в случае передачи корректного аргумента
     *
     * @dataProvider correctIdentifiersDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\NebDocumentIdentifier::match()
     * @param string $nebFullDocumentId полный идентификатор документа в формате НЭБ
     */
    public function testMatch(string $nebFullDocumentId): void
    {
        $this->assertTrue(NebDocumentIdentifier::match($nebFullDocumentId));
    }

    /**
     * Проверка метода валидации идентификатора в случае передачи некорректного аргумента
     *
     * @dataProvider invalidIdentifiersDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\NebDocumentIdentifier::match()
     * @param string $nebFullDocumentId некорректный идентификатор документа в формате НЭБ
     */
    public function testMatchInvalid(string $nebFullDocumentId): void
    {
        $this->assertFalse(NebDocumentIdentifier::match($nebFullDocumentId));
    }

    /**
     * Проверка метода, возвращающего значение полного идентификатора документа НЭБ
     *
     * @dataProvider correctIdentifiersDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\NebDocumentIdentifier::getIdentifier()
     * @param string $nebFullDocumentId полный идентификатор документа в формате НЭБ
     */
    public function testGetIdentifier(string $nebFullDocumentId): void
    {
        $identifier = new NebDocumentIdentifier($nebFullDocumentId);
        $this->assertSame($nebFullDocumentId, $identifier->getIdentifier());
    }

    /**
     * Провайдер данных, предоставляющий идентификаторы, соответствующие формату НЭБ
     *
     * @return array
     */
    public function correctIdentifiersDataProvider(): array
    {
        return [
            ['000207_000017_RU_RGDB_BIBL-0000354719'],
            ['000207_000017_RU_RGDB_BIBL'],
            ['000207_BIBL-0000354719'],
        ];
    }

    /**
     * Провайдер данных, предоставляющий идентификаторы, не соответствующие формату НЭБ
     *
     * @return array
     */
    public function invalidIdentifiersDataProvider(): array
    {
        return [
            [''],
            ['123 456 ABC'],
            ['123/456/ABC'],
        ];
    }
}
