<?php

declare(strict_types=1);

namespace Demliz\DocumentIdentifier\Tests\Identifier;

use Assert\InvalidArgumentException;
use Codeception\Test\Unit;
use Demliz\DocumentIdentifier\Identifier\RslDocumentIdentifier;

/**
 * Тест для класса идентификатора РГБ
 *
 * @author Vladimir Chalenko <rezikovka@demliz.com>
 */
class RslDocumentIdentifierTest extends Unit
{
    /**
     * Проверка получения объекта идентификатора документа РГБ в случае передачи корректного аргумента в конструктор
     *
     * @dataProvider correctIdentifiersDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\RslDocumentIdentifier::__construct()
     * @param string $rslFullDocumentId полный идентификатор документа в формате РГБ
     */
    public function testConstruct(string $rslFullDocumentId): void
    {
        $this->assertInstanceOf(
            RslDocumentIdentifier::class,
            new RslDocumentIdentifier($rslFullDocumentId)
        );
    }

    /**
     * Проверка получения объекта идентификатора документа РГБ в случае передачи некорректного аргумента в конструктор
     *
     * @dataProvider invalidIdentifiersDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\RslDocumentIdentifier::__construct()
     * @param string $rslFullDocumentId некорректный документа из библиотеки РГБ
     */
    public function testConstructWithInvalidParam(string $rslFullDocumentId): void
    {
        $this->expectException(InvalidArgumentException::class);
        new RslDocumentIdentifier($rslFullDocumentId);
    }

    /**
     * Проверка метода валидации идентификатора в случае передачи корректного аргумента
     *
     * @dataProvider correctIdentifiersDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\RslDocumentIdentifier::match()
     * @param string $rslFullDocumentId полный идентификатор документа в формате РГБ
     */
    public function testMatch(string $rslFullDocumentId): void
    {
        $this->assertTrue(RslDocumentIdentifier::match($rslFullDocumentId));
    }

    /**
     * Проверка метода валидации идентификатора в случае передачи некорректного аргумента
     *
     * @dataProvider invalidIdentifiersDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\RslDocumentIdentifier::match()
     * @param string $rslFullDocumentId некорректный идентификатор документа в формате РГБ
     */
    public function testMatchInvalid(string $rslFullDocumentId): void
    {
        $this->assertFalse(RslDocumentIdentifier::match($rslFullDocumentId));
    }

    /**
     * Проверка метода, возвращающего значение полного идентификатора документа
     *
     * @dataProvider correctIdentifiersDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\RslDocumentIdentifier::getIdentifier()
     * @param string $rslFullDocumentId полный идентификатор документа в формате РГБ
     */
    public function testGetIdentifier(string $rslFullDocumentId): void
    {
        $identifier = new RslDocumentIdentifier($rslFullDocumentId);
        $this->assertSame($rslFullDocumentId, $identifier->getIdentifier());
    }

    /**
     * Проверка метода, возвращающего код каталога РГБ, в котором расположен документ
     *
     * @dataProvider catalogCodeDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\RslDocumentIdentifier::getIdentifier()
     * @param string $rslFullDocumentId идентификатор документа из каталога РГБ
     * @param string $catalogCode ожидаемый код каталога
     */
    public function testGetCatalogCode(string $rslFullDocumentId, string $catalogCode): void
    {
        $identifier = new RslDocumentIdentifier($rslFullDocumentId);
        $this->assertSame($catalogCode, $identifier->getCatalogCode());
    }

    /**
     * Проверка метода, возвращающего ту часть полного идентификатора в формате РГБ, в которой содержится
     * непосредственно идентификатор документа
     *
     * @dataProvider documentIdDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\RslDocumentIdentifier::getIdentifier()
     * @param string $rslFullDocumentId идентификатор документа из каталога РГБ
     * @param string $documentId ожидаемый идентификатор документа в каталоге
     */
    public function testGetDocumentId(string $rslFullDocumentId, string $documentId): void
    {
        $identifier = new RslDocumentIdentifier($rslFullDocumentId);
        $this->assertSame($documentId, $identifier->getDocumentId());
    }

    /**
     * Провайдер данных, предоставляющий идентификаторы, соответствующие формату РГБ
     *
     * @return array
     */
    public function correctIdentifiersDataProvider(): array
    {
        return [
            ['rsl01098765432'],
            ['rsl02123456789'],
            ['rsl03543212345'],
        ];
    }

    /**
     * Провайдер данных, предоставляющий идентификаторы, не соответствующие формату РГБ
     *
     * @return array
     */
    public function invalidIdentifiersDataProvider(): array
    {
        return [
            [''],
            ['123 456 ABC'],
            ['123/456/ABC'],
            ['rsls0109876543'],
            ['rsl0109we65432'],
            ['rsl010987654320'],
            ['rsl010987654qq'],
            ['rs010987654320'],
            ['rs01098765432'],
        ];
    }

    /**
     * Провайдер данных, предоставляющий полные идентификаторы в формате РГБ
     * и соответствующие им коды каталогов
     *
     * @return array
     */
    public function catalogCodeDataProvider(): array
    {
        return [
            ['rslFullDocumentId' => 'rsl01098765432', 'catalogCode' => 'rsl01'],
            ['rslFullDocumentId' => 'rsl02123456789', 'catalogCode' => 'rsl02'],
            ['rslFullDocumentId' => 'rsl03543212345', 'catalogCode' => 'rsl03'],
        ];
    }

    /**
     * Провайдер данных, предоставляющий полные идентификаторы в формате РГБ
     * и соответствующие им идентификаторы документов в каталоге
     *
     * @return array
     */
    public function documentIdDataProvider(): array
    {
        return [
            ['rslFullDocumentId' => 'rsl01098765432', 'documentId' => '098765432'],
            ['rslFullDocumentId' => 'rsl02123456789', 'documentId' => '123456789'],
            ['rslFullDocumentId' => 'rsl03543212345', 'documentId' => '543212345'],
        ];
    }
}
