<?php

declare(strict_types=1);

namespace Demliz\DocumentIdentifier\Tests\Identifier;

use Assert\InvalidArgumentException;
use Codeception\Test\Unit;
use Demliz\DocumentIdentifier\Identifier\OekDocumentIdentifier;

/**
 * Тест для класса идентификатора ОЭК
 *
 * @author Vladimir Chalenko <rezikovka@demliz.com>
 */
class OekDocumentIdentifierTest extends Unit
{
    /**
     * Проверка получения объекта идентификатора документа ОЭК в случае передачи корректного аргумента в конструктор
     *
     * @dataProvider correctIdentifiersDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\OekDocumentIdentifier::__construct()
     * @param string $oekFullDocumentId полный идентификатор документа в формате ОЭК
     */
    public function testConstruct(string $oekFullDocumentId): void
    {
        $this->assertInstanceOf(
            OekDocumentIdentifier::class,
            new OekDocumentIdentifier($oekFullDocumentId)
        );
    }

    /**
     * Проверка получения объекта идентификатора документа ОЭК в случае передачи некорректного аргумента в конструктор
     *
     * @dataProvider invalidIdentifiersDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\OekDocumentIdentifier::__construct()
     * @param string $oekFullDocumentId некорректный идентификатор документа в формате ОЭК
     */
    public function testConstructWithInvalidParam(string $oekFullDocumentId): void
    {
        $this->expectException(InvalidArgumentException::class);
        new OekDocumentIdentifier($oekFullDocumentId);
    }

    /**
     * Проверка метода валидации идентификатора в случае передачи корректного аргумента
     *
     * @dataProvider correctIdentifiersDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\OekDocumentIdentifier::match()
     * @param string $oekFullDocumentId полный идентификатор документа в формате ОЭК
     */
    public function testMatch(string $oekFullDocumentId): void
    {
        $this->assertTrue(OekDocumentIdentifier::match($oekFullDocumentId));
    }

    /**
     * Проверка метода валидации идентификатора в случае передачи некорректного аргумента
     *
     * @dataProvider invalidIdentifiersDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\OekDocumentIdentifier::match()
     * @param string $oekFullDocumentId некорректный идентификатор документа в формате ОЭК
     */
    public function testMatchInvalid(string $oekFullDocumentId): void
    {
        $this->assertFalse(OekDocumentIdentifier::match($oekFullDocumentId));
    }

    /**
     * Проверка метода, возвращающего значение полного идентификатора документа
     *
     * @dataProvider correctIdentifiersDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\OekDocumentIdentifier::getIdentifier()
     * @param string $oekFullDocumentId полный идентификатор документа в формате ОЭК
     */
    public function testGetIdentifier(string $oekFullDocumentId): void
    {
        $identifier = new OekDocumentIdentifier($oekFullDocumentId);
        $this->assertSame($oekFullDocumentId, $identifier->getIdentifier());
    }

    /**
     * Проверка метода, возвращающего код каталога ОЭК, в котором расположен документ
     *
     * @dataProvider catalogCodeDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\OekDocumentIdentifier::getIdentifier()
     * @param string $oekFullDocumentId идентификатор документа из каталога ОЭК
     * @param string $catalogCode ожидаемый код каталога
     */
    public function testGetCatalogCode(string $oekFullDocumentId, string $catalogCode): void
    {
        $identifier = new OekDocumentIdentifier($oekFullDocumentId);
        $this->assertSame($catalogCode, $identifier->getCatalogCode());
    }

    /**
     * Проверка метода, возвращающего ту часть полного идентификатора в формате ОЭК, в которой содержится
     * непосредственно идентификатор документа
     *
     * @dataProvider documentIdDataProvider
     * @covers \Demliz\DocumentIdentifier\Identifier\OekDocumentIdentifier::getIdentifier()
     * @param string $oekFullDocumentId идентификатор документа из каталога ОЭК
     * @param string $documentId ожидаемый идентификатор документа в каталоге
     */
    public function testGetDocumentId(string $oekFullDocumentId, string $documentId): void
    {
        $identifier = new OekDocumentIdentifier($oekFullDocumentId);
        $this->assertSame($documentId, $identifier->getDocumentId());
    }

    /**
     * Провайдер данных, предоставляющий идентификаторы, соответствующие формату ОЭК
     *
     * @return array
     */
    public function correctIdentifiersDataProvider(): array
    {
        return [
            ['oek01098765432'],
            ['oek02123456789'],
            ['oek03543212345'],
        ];
    }

    /**
     * Провайдер данных, предоставляющий идентификаторы, не соответствующие формату ОЭК
     *
     * @return array
     */
    public function invalidIdentifiersDataProvider(): array
    {
        return [
            [''],
            ['123 456 ABC'],
            ['123/456/ABC'],
            ['oeks01098765432'],
            ['oek0109we65432'],
            ['oek010987654320'],
            ['oek010987654qq'],
            ['oe010987654320'],
            ['oe01098765432'],
            ['oek098765432'],
        ];
    }

    /**
     * Провайдер данных, предоставляющий полные идентификаторы в формате ОЭК
     * и соответствующие им коды каталогов
     *
     * @return array
     */
    public function catalogCodeDataProvider(): array
    {
        return [
            ['oekFullDocumentId' => 'oek01098765432', 'catalogCode' => 'oek01'],
            ['oekFullDocumentId' => 'oek02123456789', 'catalogCode' => 'oek02'],
            ['oekFullDocumentId' => 'oek03543212345', 'catalogCode' => 'oek03'],
        ];
    }

    /**
     * Провайдер данных, предоставляющий полные идентификаторы в формате ОЭК
     * и соответствующие им идентификаторы документов в каталоге
     *
     * @return array
     */
    public function documentIdDataProvider(): array
    {
        return [
            ['oekFullDocumentId' => 'oek01098765432', 'documentId' => '098765432'],
            ['oekFullDocumentId' => 'oek02123456789', 'documentId' => '123456789'],
            ['oekFullDocumentId' => 'oek03543212345', 'documentId' => '543212345'],
        ];
    }
}
