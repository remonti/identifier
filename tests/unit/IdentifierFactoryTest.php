<?php

declare(strict_types=1);

namespace Demliz\DocumentIdentifier\Tests;

use Codeception\Test\Unit;
use Demliz\DocumentIdentifier\Exception\UnrecognizedIdentifierException;
use Demliz\DocumentIdentifier\Identifier\NebDocumentIdentifier;
use Demliz\DocumentIdentifier\Identifier\OekDocumentIdentifier;
use Demliz\DocumentIdentifier\Identifier\RslDocumentIdentifier;
use Demliz\DocumentIdentifier\IdentifierFactory;

/**
 * Тест фабрики идентификаторов
 *
 * @author Vladimir Chalenko <rezikovka@demliz.com>
 */
class IdentifierFactoryTest extends Unit
{
    /**
     * Проверка работы метода автоматического определения типа идентификатора и создания на его основе объекта
     *
     * @dataProvider identifierDataProvider
     * @covers \Demliz\DocumentIdentifier\IdentifierFactory::create()
     * @param string $identifier идентификатор, на основе которого необходимо создать объект
     * @param string $className имя класса, экземпляр которого ожидается получить на основе переданного
     * идентификатора
     */
    public function testCreate(string $identifier, string $className): void
    {
        $this->assertInstanceOf(
            $className,
            IdentifierFactory::create($identifier)
        );
    }

    /**
     * Проверка работы метода автоматического определения типа идентификатора и создания на его основе объекта
     * в случае передачи заведомо некорректного аргумента
     *
     * @covers \Demliz\DocumentIdentifier\IdentifierFactory::create()
     */
    public function testCreateUnrecognised(): void
    {
        $this->expectException(UnrecognizedIdentifierException::class);
        IdentifierFactory::create('//77!');
    }

    /**
     * Провайдер данных для проверки корректности автоопределения типа идентификатора
     *
     * @return array
     */
    public function identifierDataProvider(): array
    {
        return [
            ['identifier' => '000207_000017_RU_RGDB_BIBL-0000354719', 'className' => NebDocumentIdentifier::class],
            ['identifier' => 'rsl01098765432', 'className' => RslDocumentIdentifier::class],
            ['identifier' => 'oek01098765432', 'className' => OekDocumentIdentifier::class],
        ];
    }
}
